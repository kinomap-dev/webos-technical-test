**OBJECTIF : Faire une application webOS qui :**

*- Permet d'afficher la liste des vignettes de 5 vidéos*

    • Les vidéos sont à récupérer depuis l'URL "https://kinomap-5a790-default-rtdb.europe-west1.firebasedatabase.app/videos.json"
    • Une vignette comprend :
      - L'image de la vidéo (en fond)
      - Le titre de la vidéo (en gros au centre)
      - La distance de la vidéo en km (en petit en bas à gauche)
      - La durée de la vidéo en min (en petit en bas à droite)

*- Permet de trier les vidéos*

    • Le tri se fait côté client
    • Tris disponibles (chacun par ordre croissant)
      - Tri alphabétique du titre (sélectionné et actif par défaut)
      - Tri par distance
      - Tri par durée
    • Présentation : un menu déroulant avec les 3 options

*- Permet de lire une vidéo (en plein écran) en cliquant dessus*

**RENDU ATTENDU : Un repository git avec un README qui détaille les étapes pour installer et lancer le projet en local**
